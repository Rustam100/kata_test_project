import java.util.*;

public class Main {

    static List<Integer> arab = Arrays.asList(100, 90, 50, 40, 10, 9, 5, 4, 1 );
    static List<String> rome = Arrays.asList("C", "XC", "L", "XL", "X", "IX", "V","IV", "I");

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(calc(sc.nextLine()));
    }

    public static String calc(String input) {
        String[] str = input.split(" ");
        int n1, n2, result;
        boolean flag;
        if (str[0].matches("^[\\d]|10$") && str[2].matches("^[\\d]|10$")) {
            n1 = Integer.parseInt(str[0]);
            n2 = Integer.parseInt(str[2]);
            flag = true;
        } else if (str[0].matches("^\\D+$") && str[2].matches("^\\D+$")){
            n1 = convertNumberFromRome(str[0]);
            n2 = convertNumberFromRome(str[2]);
            flag = false;
        } else {
            throw new CalculatorException("Numbers in different formats");
        }

        result = switch (str[1]) {
            case "+" -> n1 + n2;
            case "*" -> n1 * n2;
            case "-" -> n1 - n2;
            case "/" -> n1 / n2;
            default -> throw new CalculatorException("Operation not found");
        };
        if (flag) {
            return String.valueOf(result);
        } else {
            return convertNumberFromArab(result);
        }
    }

    private static int convertNumberFromRome(String s) {

        int pred = arab.get(rome.indexOf(String.valueOf(s.charAt(0))));
        int number = pred;
        for (int i = 1; i < s.length(); i++) {
            int n = arab.get(rome.indexOf(String.valueOf(s.charAt(i))));
            if (pred>=n) {
                number += n;
            } else {
                number += n - (2 * pred);
            }
            pred = n;
        }
        if (number > 10)
            throw new CalculatorException("Numbers cannot be greater than 10");
        return number;
    }

    private static String convertNumberFromArab(int number) {
        if (number <= 0)
            throw new CalculatorException("Roman numerals can only be positive");
        StringBuilder str = new StringBuilder();
        while (number != 0) {
            for (Integer i: arab) {
                if (i <= number) {
                    str.append(rome.get(arab.indexOf(i)));
                    number -= i;
                    break;
                }
            }
        }
        return str.toString();
    }
}

class CalculatorException extends RuntimeException{

    public CalculatorException(String message){
        super(message);
    }
}
